/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.sda.ejb;

import java.io.Serializable;
import java.util.List;
import javax.ejb.Local;
import pl.sda.ejb.model.Author;
import pl.sda.ejb.model.Book;

/**
 *
 * @author PsLgComp
 */
@Local
public interface NewSessionBeanRemote {

    public List<Book> getBooks();

    public void addBook(String title, String isbn, int releaseYear);
    
    public void addBook(Book book);
    
    default public String test(){
        return " (test interfejsu fasoli)";
    }
    
    public boolean deleteBookByID(Serializable id);
    public Author checkIfAuthorExists(String name);
}
    

