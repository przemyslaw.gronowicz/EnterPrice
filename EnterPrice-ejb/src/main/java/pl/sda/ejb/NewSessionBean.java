/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.sda.ejb;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import pl.sda.ejb.model.Author;
import pl.sda.ejb.model.Book;

/**
 *
 * @author martin
 */
@Stateless
public class NewSessionBean implements NewSessionBeanRemote {

    @PersistenceContext(unitName = "persistance_unit")
    private EntityManager em;

       
    //przykład
    //boolean result = deleteById(Product.class, new Integer(41));
    private boolean deleteById(Class<?> type, Serializable id) {
        Object persistentInstance = em.find(type, id);
        if (persistentInstance != null) {
            em.remove(persistentInstance);
            return true;
        }
        return false;
    }
    

    @Override
    public Author checkIfAuthorExists(String name) {
        if (name == null) {
            return null;
        }
        TypedQuery<Author> query = em.createQuery(
                "SELECT a FROM Author a WHERE lower(a.name) like lower(:name)", Author.class);
        query.setParameter("name", name);
        query.setMaxResults(1);
        List<Author> list = query.getResultList();
        if (list.isEmpty()) {
            return null;
        }
        return list.get(0);
    }
        

    @Override
    public boolean deleteBookByID(Serializable id) {
        return deleteById(Book.class, id);
    }

    
    public void businessMethod() {
    }

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
    @Override
    public List<Book> getBooks() {
        Query query = em.createQuery("from Book");
        return query.getResultList();
    }

    @Override
    public void addBook(String title, String isbn, int releaseYear) {
        try {
            Book book = new Book(title, isbn, releaseYear);
            em.persist(book);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    @Override
    public void addBook(Book book) {
        try {
            //book.getAuthors().forEach(a -> {if(a.getId()!=null){em.merge(a);}});
            //book.setAuthors(
            //  book.getAuthors().stream().map(a -> em.merge(a)).collect(Collectors.toList()));
            //em.persist(book);
           em.merge(book);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
