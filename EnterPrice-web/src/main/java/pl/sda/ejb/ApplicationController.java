/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.sda.ejb;

import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.inject.Named;
import pl.sda.ejb.model.Book;

/**
 *
 * @author PsLgComp
 */
@ManagedBean(name = "applicationController")
@ApplicationScoped
public class ApplicationController {

    @EJB
    private NewSessionBeanRemote bean;
    /**
     * Creates a new instance of ApplicationController
     */
    public ApplicationController() {
    }
    
    public Book newBook = new Book();
    
    public String getTest() {
        //bean.addBook("New book title", "1234", 1410);
        return bean.test();
    }
    
    public List<Book> getBooks(){
        return bean.getBooks();
    }
    public void addBook(){
        
    }
}
