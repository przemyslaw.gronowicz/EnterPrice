/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.sda.dto;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import pl.sda.ejb.model.Book;

/**
 *
 * @author RENT
 */
public class BookDTO {
    private Integer id;
    
    private String title;

    private String isbn;

    private int releaseYear;
    
    //private Collection<String> authors;
    
    private Collection<AuthorDTO> authors;

    public BookDTO() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Collection<AuthorDTO> getAuthors() {
        return this.authors;
    }

    public void setAuthors(Collection<AuthorDTO> authors) {
        this.authors = authors;
    }

    public BookDTO(String title, String isbn, int releaseYear) {
        this.title = title;
        this.isbn = isbn;
        this.releaseYear = releaseYear;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public int getReleaseYear() {
        return releaseYear;
    }

    public void setReleaseYear(int releaseYear) {
        this.releaseYear = releaseYear;
    }

    public String getAuthorsNames() {
        if (authors == null) return "";
        return authors.stream()
                .map(AuthorDTO::getName)
                .collect(Collectors.joining(", "));
    }
    
    public void setAuthorsNames(String author) {
        authors =Arrays.asList(author.split(",")).stream()
                .map(String::trim)
                .map(AuthorDTO::new)
                .collect(Collectors.toList());
    }
    

}
