/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.sda.controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import pl.sda.dto.AuthorDTO;
import pl.sda.dto.BookDTO;
import pl.sda.ejb.ApplicationController;
import pl.sda.ejb.NewSessionBeanRemote;
import pl.sda.ejb.model.Author;
import pl.sda.ejb.model.Book;


/**
 *
 * @author RENT
 */
@ManagedBean(name = "frmBook")
@RequestScoped
public class BookController {
    private BookDTO book = new BookDTO();
    private BookDTO bookToDelete;
    private List<BookDTO> bookList;// = new ArrayList<>();
    //private AuthorDTO author = new AuthorDTO();
    
    /**
     * Creates a new instance of BookController
     */
    
    @ManagedProperty(value = "#{applicationController}")
    private ApplicationController applicationController;
    
    @EJB
    private NewSessionBeanRemote bookBean;
    
    private BookDTO mapBook(Book book){
        BookDTO bookDTO = new BookDTO(book.getTitle(), book.getIsbn(), book.getReleaseYear());
        bookDTO.setId(book.getId());
        bookDTO.setAuthors(book.getAuthors().stream()
                .map(a ->{
                    AuthorDTO adto = new AuthorDTO(a.getName());
                    return adto;})
                .collect(Collectors.toList()));
        return bookDTO;
    }
    
    private Book mapBookDTO(BookDTO bookDTO){
        Book book = new Book(bookDTO.getTitle(), bookDTO.getIsbn(), bookDTO.getReleaseYear());
          book.setId(bookDTO.getId());
//        book.setAuthors(bookDTO.getAuthors().stream()
//                .map(adto -> {Author a = new Author();
//                    a.setName(adto.getName());
//                    a.setId(adto.getId());
//                    return a;})
//                .collect(Collectors.toList()));
        return book;
    }
    

    public ApplicationController getApplicationController() {
        return applicationController;
    }

    public void setApplicationController(ApplicationController applicationController) {
        this.applicationController = applicationController;
    }
    public BookController() {
    }
    
    public void setTitle(String title){
       this.book.setTitle(title);
    }
    
    public String getTitle(){
        return book.getTitle();
    }
    
    public void setIsbn(String isbn){
        this.book.setIsbn(isbn);
    }
    
    public String getIsbn(){
        return this.book.getIsbn();
    }
    
    public void setReleaseYear(String releaseYear){
        this.book.setReleaseYear(Integer.parseInt(releaseYear));
    }
    
    public String getReleaseYear(){
        return Integer.toString(this.book.getReleaseYear());
    }
    
    public void addBook() {
        Book book = mapBookDTO(this.book);
        book.setAuthors(
                this.book.getAuthors().stream()
                        .map(adto -> {
                            Collection<Book> authorsBooks = null;
                            Author a = bookBean.checkIfAuthorExists(adto.getName());
                            if (a == null) {//autora nie ma w bazie
                                a = new Author();
                                a.setName(adto.getName());
                                a.setId(adto.getId());
                                authorsBooks = new ArrayList<>();
                            } else {//autor jest w bazie
                                authorsBooks = a.getBooks();
                                if (authorsBooks == null) {
                                    authorsBooks = new ArrayList<>();
                                }
                            }
                            authorsBooks.add(book);
                            a.setBooks(authorsBooks);
                            return a;
                        })
                        .collect(Collectors.toList()));
        bookBean.addBook(book);
        System.out.println(book.getTitle());
        
    }
    
    public String getAuthorsNames(){
        return this.book.getAuthorsNames();
    }
    
    public void setAuthorsNames(String author){
        this.book.setAuthorsNames(author);
    }

//    
//    public AuthorDTO getAuthor(){
//        return this.author;
//    }
//    
//    public void SetAuthor(AuthorDTO author){
//        this.author = author;
//    }
//    
    public void setBookToDelete(BookDTO book){
        this.bookToDelete = book;
    }

    public BookDTO getBookToDelete() {
        return bookToDelete;
    }
    
    
    
    
    
    public void delete(){
        boolean postback = FacesContext.getCurrentInstance().isPostback();
        if (postback){
            System.out.println("deleting book -> id = " + bookToDelete.getId()
                    + " (" + bookToDelete.getTitle() + ")");
            bookBean.deleteBookByID(bookToDelete.getId());
            System.out.println("postback = thrue");
          
        }else{
            System.out.println("postback = false");
            
        }
    }
    
    public List<BookDTO> getBooks() {
        bookList = bookBean.getBooks().stream().map(this::mapBook).collect(Collectors.toList());
        return bookList;
    }
}
